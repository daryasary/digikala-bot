from django.db import models
from django.utils.translation import ugettext as _
from utils.base_models import BaseModel


class Seller(BaseModel):
    name = models.CharField(max_length=100, verbose_name=_('Seller Name'))
    seller_uid = models.CharField(unique=True, max_length=20, verbose_name=_('Seller Code'))
    seller_panel_id = models.PositiveIntegerField(unique=True, verbose_name=_('Seller ID'))
    priority = models.PositiveIntegerField(unique=True, verbose_name=_('Priority'))
    panel_username = models.CharField(max_length=256, verbose_name=_('Panel Username'))
    panel_password = models.CharField(max_length=256, verbose_name=_('Panel Password'))
    is_active = models.BooleanField(default=True, verbose_name=_('Is Active'))

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Seller')
        verbose_name_plural = _('Sellers')
