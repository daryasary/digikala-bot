from django.contrib import admin
from seller.models import Seller
from utils.base_models import BadeAdmin


class SellerAdmin(BadeAdmin):
    list_display = ('name', 'seller_uid', 'seller_panel_id', 'priority', 'is_active')
    exclude = ()

    def has_add_permission(self, request):
        num_objects = self.model.objects.count()
        if num_objects >= 1:
            return False
        else:
            return True


admin.site.register(Seller, SellerAdmin)
