from django.urls import path
from seller.views import turn_off_celery, turn_on_celery

app_name = 'seller'

urlpatterns = [
    path('enable_celery/', turn_on_celery),
    path('disable_celery/', turn_off_celery)
]
