from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from django.urls import reverse
from django.views.decorators.http import require_http_methods
from utils.system_control import enable_celery, disable_celery


@login_required
@require_http_methods(["GET"])
def turn_on_celery(request):
    enable_celery()
    return redirect(reverse('admin:index'))


@login_required
@require_http_methods(["GET"])
def turn_off_celery(request):
    disable_celery()
    return redirect(reverse('admin:index'))
