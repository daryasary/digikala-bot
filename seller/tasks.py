from __future__ import absolute_import, unicode_literals
from sellers.celery_app import app
from celery.utils.log import get_task_logger
from crawler.seller_panel_crawler import SellerPanelCrawler
from celery_once import QueueOnce

logger = get_task_logger(__name__)


#
# @app.task(queue='PanelQ')
# def run_price_change_tasks():
#     if is_celery_enable():
#         products = Product.objects.filter(title__isnull=True).values_list('id', flat=True)
#         for pid in products:
#             print("Fetch data for PK:" + str(pid))
#             crawl_product_detail.delay(pid)
#
#         if not len(products):
#             print("Nothing to crawl...")
#

# @app.task(queue='PanelQ', base=QueueOnce, once={'keys': ['product_pk']})
@app.task(queue='PanelQ')
def reduce_product_price(product_pk, reduced_price, report_pk):
    print('Starting to Reduce')
    try:
        crawler = SellerPanelCrawler(product_pk, reduced_price, report_pk)
        crawler.reduce_price()
        status = True, "Done"
    except Exception as e:
        status = False, str(e)
        crawler.driver.quit()
    return status
