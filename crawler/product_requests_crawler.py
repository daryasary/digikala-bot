import random
from urllib.parse import urlparse
from bs4 import BeautifulSoup
import requests
import re
from celery_once import AlreadyQueued

from seller.tasks import reduce_product_price
from crawler.objects import VariantPrice, Variant, ProductPageSellers
import json
from report.models import Report


class ProductRequestsCrawler:

    def __init__(self, product, from_admin=False):
        self.product = product
        print('-- ProductRequestsCrawler {} - {}'.format(self.product.product_uid, str(self.product.variant_id)))
        headers = {
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1)'
                          ' AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.1 Safari/537.36'}
        self.r = requests.get(self.product.product_url, headers=headers)
        self.soup = BeautifulSoup(self.r.content, features="html.parser")
        if not from_admin:
            if not self.is_product_out_of_stock():
                self.lowest_price_seller = self.get_lowest_price_seller()
                self.save_product_color()

    def get_variant_object_list(self):
        variant_object_list = []
        pattern = re.compile('var\svariants\s=\s(.*?);')
        script = self.soup.find("script", text=pattern)
        if script:
            match = pattern.search(script.text)
            if match:
                variants = match.group(1)
                variants_json = json.loads(variants)
                for variant in variants_json:
                    variant_object_list.append(Variant(int(variant), variants_json[variant]))
                return variant_object_list

    def save_product_color(self):
        variant_object_list = self.get_variant_object_list()
        variant = filter(lambda x: x.variant_id == self.product.variant_id, variant_object_list)
        variant = next(variant, None)
        if variant is not None:
            if 'color' in variant.json:
                if 'id' in variant.json['color']:
                    self.product.color_id = variant.json['color']['id']
                    self.product.save()
                else:
                    self.product.color_id = None
                    self.product.save()
        else:
            self.product.status = 10
            self.product.save()

    def get_same_color_filtered_variants_ids(self):
        variant_object_list = self.get_variant_object_list()
        same_color_filtered_variants = filter(lambda x: x.json['color'] != [] and
                                                        x.json['color']['id'] == self.product.color_id,
                                              variant_object_list)
        same_color_filtered_variants_ids = [variant.variant_id for variant in same_color_filtered_variants]
        return same_color_filtered_variants_ids

    def lowest_price(self):
        if self.is_product_out_of_stock():
            return 0
        else:
            buttons_prices = []
            # add_card_buttons = self.soup.select('c-btn-seller-add-cart js-variant-add-to-cart js-btn-add-to-cart')
            add_card_buttons = self.soup.find_all('a', attrs={'class': 'c-btn-seller-add-cart'})
            for button in add_card_buttons:
                data_event_label = button.get('data-event-label')
                price = int(re.search(r'\d+', data_event_label).group())
                add_to_card_href = button.get('href')
                variant_id = int(add_to_card_href.split('/')[3])
                buttons_prices.append(VariantPrice(variant_id, price))
            buttons_prices = sorted(buttons_prices, key=lambda x: x.price)
            # for i in buttons_prices:
            #     print('{} - {}'.format(i.price, i.variant_id))
            if self.product.color_id is not None:
                same_color_filtered_variants_ids = self.get_same_color_filtered_variants_ids()
                buttons_prices = [variant for variant in buttons_prices if
                                  variant.variant_id in same_color_filtered_variants_ids]
            print('---')
            for i in buttons_prices:
                print('{} - {}'.format(i.price, i.variant_id))
            return buttons_prices[0] if len(buttons_prices) else None

    def get_product_title(self):
        title = self.soup.find('h1', attrs={'class': 'c-product__title'})
        return title.text.strip()

    def get_product_uid(self):
        url_path = urlparse(self.product.product_url).path
        stripped_path = url_path.split('/')
        product_uid = stripped_path[2]
        product_id = int(re.search(r'\d+', product_uid).group())
        return product_id, product_uid

    def get_seller_uid(self, seller):
        parsed_url = urlparse(self.get_seller_url(seller))
        path = parsed_url.path
        try:
            uid = path.replace('seller', '')
            return uid.replace('/', '')
        except TypeError:
            return 'digi_kala'

    def get_sellers_row(self):
        """extract sellers listed on the product page on digikala site"""
        sellers_table = self.soup.find('div', attrs={'class': 'c-table-suppliers__body'})
        sellers = sellers_table.find_all('div', attrs={'class': 'c-table-suppliers__row'})
        return sellers

    @staticmethod
    def get_seller_name(seller):
        return seller.find('a', attrs={'class': 'btn-link-spoiler'}).text

    @staticmethod
    def get_seller_url(seller):
        try:
            return seller.find('a').get('href')
        except AttributeError:
            return 'digikala.com'

    @staticmethod
    def get_seller_product_variant_id(seller):
        add_to_card_href = seller.find('a', attrs={'class': 'c-btn-seller-add-cart'}).get('href')
        variant_id = int(add_to_card_href.split('/')[3])
        return variant_id

    @staticmethod
    def get_seller_price(seller):
        data_event_label = seller.find('div', attrs={'class': 'c-price__value'}).text
        # return int(re.search(r'\d+', data_event_label).group())
        return int(re.search(r'\d+(,\d+)*', data_event_label).group().replace(',', '')) * 10

    def get_sorted_sellers_by_price(self):
        """Get sellers of product sorted, according to the price"""
        crawled_prices = self.get_seller()
        if self.product.color_id is not None:
            same_color_filtered_variants_ids = self.get_same_color_filtered_variants_ids()
            crawled_prices = [seller for seller in crawled_prices if
                              seller.variant_id in same_color_filtered_variants_ids]
        sorted_sellers = sorted(crawled_prices, key=lambda x: x.price)
        self.product.lowest_spotted_price = sorted_sellers[0].price
        self.product.save()
        return sorted_sellers

    def get_sorted_sellers_by_position(self):
        """Get sellers of product sorted, according to position"""
        crawled_prices = self.get_seller()
        if self.product.color_id is not None:
            same_color_filtered_variants_ids = self.get_same_color_filtered_variants_ids()
            crawled_prices = [seller for seller in crawled_prices if
                              seller.variant_id in same_color_filtered_variants_ids]
        self.product.lowest_spotted_price = crawled_prices[0].price
        self.product.save()
        return crawled_prices

    def get_lowest_price_seller(self):
        """Return submitted price of first seller from sorted sellers list"""
        # TODO: May name should be changed to `get_first_seller`
        sorted_sellers = self.get_sorted_sellers_by_price()
        return sorted_sellers[0]

    def get_second_lowest_price_seller(self):
        sorted_sellers = self.get_sorted_sellers_by_price()
        return sorted_sellers[1]

    def is_second_lowest_price_seller_available(self):
        sorted_sellers = self.get_sorted_sellers_by_price()
        if len(sorted_sellers) > 1:
            return True
        else:
            self.product.is_lowest_price = True
            self.product.status = 2
            self.product.save()
            return False

    def get_self_seller(self):
        sorted_sellers = self.get_sorted_sellers_by_price()
        self_seller = [seller for seller in sorted_sellers if seller.seller_uid == self.product.seller.seller_uid]
        if self_seller:
            return self_seller[0]

    def get_seller(self):
        crawled_prices = []

        for seller in self.get_sellers_row():
            uid = self.get_seller_uid(seller)
            price = self.get_seller_price(seller)
            variant_id = self.get_seller_product_variant_id(seller)
            crawled_prices.append(ProductPageSellers(uid, variant_id, price))
        return crawled_prices

    def is_seller_available(self):
        """Check if seller is listed on product page sellers on digikala site"""
        sellers = self.get_seller()
        sellers_uids = [seller.seller_uid for seller in sellers]
        is_available = self.product.seller.seller_uid in sellers_uids
        if not is_available:
            self.product.status = 10
            self.product.save()
            # check_alert.apply_async((self.product.pk,), countdown=10)
        return is_available

    def is_product_out_of_stock(self):
        """If product is out of stock there is no need to being crawled or
        updated anymore"""
        out_of_stock = self.soup.select_one('div.c-product__status-bar.c-product__status-bar--out-of-stock')
        if out_of_stock is not None:
            self.product.status = 9
            self.product.save()
            # check_alert.apply_async((self.product.pk,), countdown=30)
            return True
        else:
            # check_alert.apply_async((self.product.pk,), countdown=30)
            return False

    def is_seller_lowest_price(self):
        # revoke_alert.delay(self.product.pk)
        is_lowest = self.lowest_price_seller.seller_uid == self.product.seller.seller_uid
        if is_lowest:
            self.product.is_lowest_price = True
            self.product.status = 3
            self.product.save()
        else:
            self.product.is_lowest_price = False
            self.product.status = 4
            self.product.save()
        return is_lowest

    def is_exceeding_min_price(self, round_reduced_price_in_rials):
        is_exceeding = self.product.minimum_price > round_reduced_price_in_rials
        if is_exceeding:
            self.product.status = 6
            self.product.save()
        return is_exceeding

    def get_reduced_price(self, seller):
        offset_price = random.choice(self.product.offset_prices)[0]
        reduced_price_in_rials = seller.price - offset_price
        divided_reduced_price_in_rials = int(reduced_price_in_rials / 1000)
        round_reduced_price_in_rials = int(divided_reduced_price_in_rials * 1000)
        return {
            'offset_price': offset_price,
            'reduced_price_in_rials': reduced_price_in_rials,
            'divided_reduced_price_in_rials': divided_reduced_price_in_rials,
            'round_reduced_price_in_rials': round_reduced_price_in_rials
        }

    def get_report(self, reduced_prices, seller):
        report = Report(product=self.product,
                        market_lowest_price=seller.price,
                        offset_price=reduced_prices['offset_price'],
                        minimum_price=self.product.minimum_price,
                        reduced_price=reduced_prices['reduced_price_in_rials'],
                        divided_reduced_price_in_rials=reduced_prices['divided_reduced_price_in_rials'],
                        round_reduced_price=reduced_prices['round_reduced_price_in_rials'])
        return report

    def change_product_price(self, report):
        print('Changing')
        try:
            report.status = 1
            report.save()
            reduce_product_price.delay(self.product.pk, report.round_reduced_price, report.pk)
        except AlreadyQueued:
            print('-- Already Reducing: {} - {}'.format(self.product.product_uid,
                                                        self.product.variant_id))

    def jump_to_exceeded_price(self):
        report = Report(product=self.product,
                        market_lowest_price=self.lowest_price_seller.price,
                        offset_price=0,
                        minimum_price=self.product.minimum_price,
                        reduced_price=0,
                        divided_reduced_price_in_rials=0,
                        round_reduced_price=self.product.exceed_price)
        try:
            report.status = 1
            report.save()
            reduce_product_price.delay(self.product.pk, self.product.exceed_price, report.pk)
        except AlreadyQueued:
            print('-- Already Exceeding: {} - {}'.format(self.product.product_uid,
                                                         self.product.variant_id))

    def is_seller_first(self):
        """Check if seller is first listed on digikala or not"""
        sellers = self.get_sorted_sellers_by_position()
        is_first = bool(self.product.seller.seller_uid == sellers[0].seller_uid)
        if is_first:
            self.product.status = 20
            self.product.save()
        return is_first

    def can_auto_change_price(self):
        can_change = self.product.can_auto_change_price
        if not can_change:
            self.product.status = 5
            self.product.save()
        return can_change

    def can_jump_to_exceed_price(self):
        if self.product.exceed_price is not None:
            self_seller = self.get_self_seller()
            can_jump = self_seller.price < self.product.exceed_price
            if not can_jump:
                self.product.status = 11
                self.product.save()
            return can_jump
        else:
            self.product.status = 12
            self.product.save()
            return False

    def check_last_change_applied(self):
        last_report = self.product.report_set.last()
        if last_report is None:
            return True
        applied = bool(last_report.round_reduced_price == self.get_self_seller().price)
        if not applied:
            self.product.status = 7  # Price is not affected in digikala site
            self.product.save()
        return applied

    def is_price_difference_much(self):
        if self.product.price_difference is not None:
            second_lowest_seller = self.get_second_lowest_price_seller()
            self_seller = self.get_self_seller()
            price_difference = second_lowest_seller.price - self_seller.price
            is_difference_much = self.product.price_difference < price_difference
            return is_difference_much
        return False

    def calculate_check_change_price(self, seller):
        reduced_prices = self.get_reduced_price(seller)
        if not self.is_exceeding_min_price(reduced_prices['round_reduced_price_in_rials']):
            report = self.get_report(reduced_prices, seller)
            self.change_product_price(report)
        else:
            if self.can_jump_to_exceed_price():
                self.jump_to_exceeded_price()

    def check_prices(self):
        self.product.status = 1
        self.product.save()
        if not self.is_product_out_of_stock():
            if self.is_seller_available():
                if self.is_seller_lowest_price():
                    if self.is_second_lowest_price_seller_available():
                        if self.is_price_difference_much():
                            second_lowest_price_seller = self.get_second_lowest_price_seller()
                            self.calculate_check_change_price(second_lowest_price_seller)
                else:
                    if self.can_auto_change_price():
                        self.calculate_check_change_price(self.lowest_price_seller)

    def check_position(self):
        # Change product status to `crawling`
        self.product.status = 1
        self.product.save()
        # Check if product is not out of stock
        if not self.is_product_out_of_stock():
            # Check if seller is listed on product sellers
            if self.is_seller_available():
                # Check if last requested price is applied on site
                # if self.check_last_change_applied():

                # Check if seller position is not first of sellers
                if not self.is_seller_first():
                    # Check if can change the product price automatically
                    if self.can_auto_change_price():
                        # Reduce price and wait to be applied on digikala site
                        self.calculate_check_change_price(self.get_lowest_price_seller())
