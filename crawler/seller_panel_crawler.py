import time
from selenium import webdriver
from selenium.common.exceptions import ElementNotVisibleException, StaleElementReferenceException, WebDriverException
import pickle
from django.conf import settings
from selenium.webdriver.common.keys import Keys
from unidecode import unidecode
from report.models import Report
from product.models import Product


class SellerPanelCrawler:

    def __init__(self, product_pk, reduced_price, report_pk):
        self.product = Product.objects.get(pk=product_pk)
        self.reduced_price = reduced_price
        self.report = Report.objects.get(pk=report_pk)
        options = webdriver.ChromeOptions()
        print('-- Reducing Product Price: {} - Variant ID: {}'.format(self.product.product_uid,
                                                                      str(self.product.variant_id)))
        options.add_argument('--headless')
        options.add_argument('--disable-gpu')
        options.add_argument('--no-sandbox')
        options.add_argument("--start-maximized")
        self.driver = webdriver.Chrome(settings.CHROME_DRIVER_PATH, chrome_options=options)
        self.driver.set_window_size(1440, 900)
        self.driver.maximize_window()
        self.login()

    def login_with_credentials(self):
        self.driver.get(settings.DIGIKALA_PANEL_LOGIN_URL)
        username_field = self.driver.find_element_by_xpath('//*[@id="login-form"]/div[1]/div/div/input')
        username_field.clear()
        username_field.send_keys(self.product.seller.panel_username)
        password_field = self.driver.find_element_by_xpath('//*[@id="login-form"]/div[2]/div/div/input')
        password_field.clear()
        password_field.send_keys(self.product.seller.panel_password)
        login_button = self.driver.find_element_by_xpath('//*[@id="btnSubmit"]')
        login_button.click()
        pickle.dump(self.driver.get_cookies(), open(
            settings.COOKIES_DIR + "%s_cookies.pkl" % self.product.seller.seller_uid, "wb"))

    def login(self):
        try:
            self.driver.get(settings.DIGIKALA_PANEL_URL)
            cookies = pickle.load(open(settings.COOKIES_DIR + "%s_cookies.pkl" % self.product.seller.seller_uid, "rb"))
            for cookie in cookies:
                self.driver.add_cookie(cookie)
            self.driver.refresh()
            self.get_edit_price_page()
            if self.driver.current_url != settings.DIGIKALA_EDIT_PRODUCT_PRICE_URL:
                self.login_with_credentials()
        except (OSError, IOError) as e:
            self.login_with_credentials()

    def get_edit_price_page(self):
        edit_page_url = settings.DIGIKALA_EDIT_PRODUCT_PRICE_URL
        self.driver.get(edit_page_url)
        if self.driver.current_url != edit_page_url:
            self.driver.get(edit_page_url)
        self.driver.refresh()

    def save_stock_available(self):
        left_stock_available = self.driver.find_element_by_xpath(
            '/html/body/main/div/div/div[3]/section[2]/div[2]/table/tbody/tr/td[14]/input[1]').get_attribute('value')
        # self.product.stock_available = int(unidecode(left_stock_available))
        self.product.stock_available = left_stock_available
        self.product.save()

    def search_product(self):
        search_field = self.driver.find_element_by_xpath('//*[@id="searchForm"]/div/div[2]/div/input')
        search_field.clear()
        search_field.send_keys(self.product.product_id)
        variant_field = self.driver.find_element_by_xpath('//*[@id="searchForm"]/div/div[3]/div/input')
        variant_field.clear()
        variant_field.send_keys(self.product.variant_id)
        variant_field.send_keys(Keys.RETURN)
        # search_button = self.driver.find_element_by_xpath('//*[@id="submitButton"]')
        # search_button.click()

    def find_click_edit_price_button(self):
        try:
            edit_price_button = self.driver.find_element_by_xpath(
                '/html/body/main/div/div/div[3]/section[2]/div[2]/table/tbody/tr[1]/td[20]/button[1]')
            self.driver.execute_script("arguments[0].scrollIntoView();", edit_price_button)
            time.sleep(5)
            edit_price_button.click()
        except (StaleElementReferenceException, WebDriverException):
            edit_price_button = self.driver.find_element_by_xpath(
                '/html/body/main/div/div/div[3]/section[2]/div[2]/table/tbody/tr/td[20]/button[1]')
            self.driver.execute_script("arguments[0].scrollIntoView();", edit_price_button)
            time.sleep(5)
            edit_price_button.click()

    def enter_new_price(self):
        try:
            sell_price_element = self.driver.find_element_by_xpath(
                '/html/body/main/div/div/div[3]/section[2]/div[2]/table/tbody/tr/td[12]/input[1]')
            sell_price_element.clear()
            sell_price_element.send_keys(self.reduced_price)
        except (StaleElementReferenceException, WebDriverException):
            sell_price_element = self.driver.find_element_by_xpath(
                '/html/body/main/div/div/div[3]/section[2]/div[2]/table/tbody/tr[1]/td[12]/input[1]')
            sell_price_element.clear()
            sell_price_element.send_keys(self.reduced_price)

    def click_save_changes(self):
        try:
            submit_changes_button = self.driver.find_element_by_xpath(
                '/html/body/main/div/div/div[3]/section[2]/div[2]/table/tbody/tr/td[20]/div/button[1]')
            submit_changes_button.click()
        except (StaleElementReferenceException, WebDriverException):
            submit_changes_button = self.driver.find_element_by_xpath(
                '/html/body/main/div/div/div[3]/section[2]/div[2]/table/tbody/tr[1]/td[20]/div/button[1]')
            submit_changes_button.click()

    def reduce_price(self):
        self.product.status = 7
        self.product.save()
        self.get_edit_price_page()
        self.search_product()
        self.find_click_edit_price_button()
        self.enter_new_price()
        self.save_stock_available()
        self.click_save_changes()
        time.sleep(1)
        self.report.status = 2
        self.report.save()
        self.product.status = 8
        self.product.save()
        self.driver.quit()
