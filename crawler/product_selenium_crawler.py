import time
from urllib.parse import urlparse
from selenium import webdriver
from selenium.common.exceptions import ElementNotVisibleException, NoSuchElementException
import re
from django.conf import settings
from product import tasks


class ProductCrawler:

    def __init__(self, product):
        self.crawled_prices = {}
        self.product = product
        options = webdriver.ChromeOptions()
        options.add_argument('--headless')
        options.add_argument('--disable-gpu')
        options.add_argument('--no-sandbox')
        options.add_argument("--start-maximized")
        self.driver = webdriver.Chrome(settings.CHROME_DRIVER_PATH, chrome_options=options)
        self.driver.get(self.product.product_url)
        self.driver.set_window_size(1440, 900)
        # self.driver.implicitly_wait(10)

    @staticmethod
    def get_seller_uid(url):
        parsed_url = urlparse(url)
        path = parsed_url.path
        uid = path.replace('seller', '')
        return uid.replace('/', '')

    def check_prices(self):
        self.product.status = 1
        self.product.save()
        try:
            more_button = self.driver.find_element_by_xpath('//*[@id="content"]/div/div/div[2]/div[2]/div[4]/button')
            self.driver.execute_script('window.scrollTo(0, ' + str(more_button.location['y']) + ');')
            time.sleep(5)
            more_button.click()
        except ElementNotVisibleException:
            pass
        except NoSuchElementException:
            pass
        buttons_prices = []
        add_card_buttons = self.driver.find_elements_by_xpath(
            "//a[@class='btn-default js-variant-add-to-cart']")
        for button in add_card_buttons:
            data_event_label = button.get_attribute('data-event-label')
            buttons_prices.append(int(re.search(r'\d+', data_event_label).group()))

        sellers_list = self.driver.find_elements_by_xpath(
            "//div[@class='c-table-suppliers__row js-supplier in-filter in-list']")
        if len(sellers_list) == 1:
            self.product.status = 2
            self.product.save()
        for seller in sellers_list:
            name = seller.find_element_by_class_name("btn-link-spoiler").text
            url = seller.find_element_by_class_name("btn-link-spoiler").get_attribute('href')
            uid = self.get_seller_uid(url)
            data_event_label = seller.find_element_by_xpath(
                ".//a[@class='btn-default js-variant-add-to-cart']").get_attribute('data-event-label')
            price = int(re.search(r'\d+', data_event_label).group())
            self.crawled_prices[uid] = price
        sorted_prices = [(k, v) for k, v in self.crawled_prices.items()]
        lowest_price_seller = sorted_prices[0]
        if lowest_price_seller[0] == self.product.seller.seller_uid:
            self.product.is_lowest_price = True
            self.product.status = 3
            self.product.save()
        else:
            self.product.is_lowest_price = False
            self.product.lowest_spotted_price = buttons_prices[0]
            self.product.status = 4
            self.product.save()
            reduced_price_in_rials = lowest_price_seller[1] - self.product.offset_price
            if self.product.minimum_price < reduced_price_in_rials:
                if self.product.can_auto_change_price:
                    tasks.reduce_product_price.delay(self.product.pk, reduced_price_in_rials)
                else:
                    self.product.status = 5
                    self.product.save()
            else:
                self.product.status = 6
                self.product.save()
        self.driver.quit()
