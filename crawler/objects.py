class VariantPrice:
    def __init__(self, variant_id, price):
        self.variant_id = variant_id
        self.price = price


class Variant:
    def __init__(self, variant_id, json):
        self.variant_id = variant_id
        self.json = json


class ProductPageSellers:
    def __init__(self, seller_uid, variant_id, price):
        self.seller_uid = seller_uid
        self.variant_id = variant_id
        self.price = price

