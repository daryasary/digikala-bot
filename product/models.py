from django.contrib.postgres.fields import ArrayField
from django.db import models
from seller.models import Seller
from django.utils.translation import ugettext as _
from utils.base_models import BaseModel
from django_celery_beat.models import PeriodicTask, IntervalSchedule
from django.db.models.signals import post_delete


class Product(BaseModel):

    STATUS_CHOICES = (
        (0, _('Not Crawled')),
        (1, _('Crawling')),
        (2, _('One Seller Is Present')),
        (3, _('Is Lowest Price')),
        (4, _('Is Not Lowest Price')),
        (5, _('Is Not Lowest Price Auto Price Change Is Disabled')),
        (6, _('Exceed Minimum Price')),
        (7, _('Changing Price In Seller Panel')),
        (8, _('Price Changed In Seller Panel')),
        (9, _('Product Out Of Stuck')),
        (10, _('Seller Out Of Stuck')),
        (11, _('Is Exceeding Price')),
        (12, _('Exceeding Not Setted')),
        (20, _('Seller is at the top')),
    )
    title = models.CharField(max_length=500, verbose_name=_('Product Title'), blank=True, null=True)
    product_url = models.URLField(verbose_name=_('URL'))
    product_id = models.PositiveIntegerField(verbose_name=_('Product ID'), blank=True, null=True)
    product_uid = models.CharField(unique=False, max_length=100, verbose_name=_('UID'), blank=True, null=True)
    variant_id = models.PositiveIntegerField(unique=True, verbose_name=_('Variant ID'))
    color_id = models.PositiveIntegerField(null=True, blank=True, verbose_name=_('Color ID'))
    stock_available = models.PositiveIntegerField(null=True, blank=True, verbose_name=_('Left Available'))
    seller = models.ForeignKey(to=Seller, on_delete=models.CASCADE, verbose_name=_('Seller'), related_name='products')
    status = models.SmallIntegerField(choices=STATUS_CHOICES, default=0, verbose_name=_('Status'))
    minimum_price = models.PositiveIntegerField(verbose_name=_('Min Price'), help_text=_('In Rials'))
    exceed_price = models.PositiveIntegerField(null=True, blank=True, verbose_name=_('Exceed Price'),
                                               help_text=_('In Rials'))
    price_difference = models.PositiveIntegerField(null=True, blank=True,
                                                   verbose_name=_('Price Difference'), help_text=_('In Rials'))
    offset_prices = ArrayField(ArrayField(models.PositiveIntegerField(default=1000, verbose_name=_('Price Offset'),
                                                                      help_text=_('In Rials')), size=1))
    is_active = models.BooleanField(default=False, verbose_name=_('Is Active'))
    can_auto_change_price = models.BooleanField(default=False, verbose_name=_('Auto Change'))
    is_lowest_price = models.NullBooleanField(null=True, blank=True, verbose_name=_('Is Lowest Price'))
    lowest_spotted_price = models.PositiveIntegerField(null=True, blank=True, verbose_name=_('Lowest Price'),
                                                       help_text=_('In Rials'))
    periodic_task = models.ForeignKey(null=True, blank=True, to=PeriodicTask, on_delete=models.SET_NULL)
    interval = models.ForeignKey(null=True, blank=True, to=IntervalSchedule, on_delete=models.SET_NULL,
                                 verbose_name=_('Interval'), related_name='product_interval')

    def __str__(self):
        return self.product_url

    class Meta:
        verbose_name = _('Product')
        verbose_name_plural = _('Products')


class Delay(IntervalSchedule):
    product = models.ForeignKey(to=Product, on_delete=models.CASCADE, verbose_name=_('Product'), related_name='delays')


def subscription_handler(sender, instance, **kwargs):
    if instance.periodic_task is not None:
        instance.periodic_task.delete()


post_delete.connect(subscription_handler, sender=Product, dispatch_uid="subscription_handler")