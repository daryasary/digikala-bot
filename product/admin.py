from django.forms import TextInput, NumberInput
from django.contrib.postgres.fields import ArrayField
from django.urls import reverse
from django.utils.html import format_html
from crawler.product_requests_crawler import ProductRequestsCrawler
from babel.numbers import format_decimal
from django.contrib import admin
from product.models import Product, Delay
from django.utils.translation import ugettext as _

from product.tasks import crawl_product_detail
from utils.base_models import BadeAdmin
from django.utils import timezone
from django.db import models


class DelayInlineAdmin(admin.StackedInline):
    model = Delay


class ProductAdmin(BadeAdmin):
    list_display = ('product_link', 'jupdated_date', 'variant_id', 'formatted_min_price', 'offset_prices',
                    'exceed_price', 'formatted_lowest_price', 'is_active', 'can_auto_change_price', 'simple_status',
                    'is_lowest_price', 'stock_available', 'edit_action')

    list_filter = ('seller', 'is_active', 'can_auto_change_price', 'is_lowest_price', 'status')
    search_fields = ('title', 'variant_id', 'product_uid', 'product_id', 'product_url', 'stock_available',)
    actions = ('disable_selected', 'enable_selected', 'disable_auto_change_selected', 'enable_auto_change_selected')
    exclude = ('periodic_task',)
    ordering = ('-is_active',)
    inlines = (DelayInlineAdmin,)
    formfield_overrides = {
        models.PositiveIntegerField: {'widget': NumberInput(attrs={'size': '40'})},
        ArrayField: {'widget': TextInput(attrs={'size': '40'})},
    }

    def __init__(self, *args, **kwargs):
        super(ProductAdmin, self).__init__(*args, **kwargs)
        self.readonly_fields += ('title', 'product_id', 'product_uid', 'is_lowest_price', 'lowest_spotted_price',
                                 'status', 'stock_available', 'color_id')

    def save_model(self, request, obj, form, change):
        super(ProductAdmin, self).save_model(request, obj, form, change)
        # crawl_product_detail(obj.id)

    def edit_action(self, obj):
        return format_html(
            '<a href="{}" class ="button">Edit</a>', reverse('admin:product_product_change', args=[obj.pk])
        )

    edit_action.short_description = _('Edit')

    def product_link(self, obj):
        return format_html('<a href="{0}" target="_blank">{1}</a>', obj.product_url, obj.title)

    product_link.short_description = _('Product Title')

    def jupdated_date(self, obj):
        return timezone.localtime(obj.updated_date).strftime("%Y/%m/%d %H:%M:%S")

    jupdated_date.short_description = _('Updated Date')

    def formatted_min_price(self, obj):
        return format_decimal(obj.minimum_price, locale='en_US')

    formatted_min_price.short_description = _('Min Price')

    def formatted_price_offset(self, obj):
        return format_decimal(0, locale='en_US')

    formatted_price_offset.short_description = _('Price Offset')

    def formatted_lowest_price(self, obj):
        if obj.lowest_spotted_price is None:
            return format_decimal(0, locale='en_US')
        return format_decimal(obj.lowest_spotted_price, locale='en_US')

    formatted_lowest_price.short_description = _('Lowest Price')

    def simple_status(self, obj):
        if obj.status == 0:
            return _('Not Crawled')
        if obj.status == 1:
            return _('Crawling')
        if obj.status == 9:
            return _('Product Out Of Stock')
        if obj.status == 10:
            return _('Seller Out Of Stock')
        else:
            return obj.get_status_display()

    simple_status.short_description = _('Status')

    def disable_selected(self, request, queryset):
        for obj in queryset:
            self.enable_periodic_task(obj, False)
        queryset.update(is_active=False)

    disable_selected.short_description = _('Disable Selected Products')

    def enable_selected(self, request, queryset):
        for obj in queryset:
            self.enable_periodic_task(obj, True)
        queryset.update(is_active=True)

    enable_selected.short_description = _('Enable Selected Products')

    def disable_auto_change_selected(self, request, queryset):
        queryset.update(can_auto_change_price=False)

    disable_auto_change_selected.short_description = _('Disable Auto Change Selected Products')

    def enable_auto_change_selected(self, request, queryset):
        queryset.update(can_auto_change_price=True)

    enable_auto_change_selected.short_description = _('Enable Auto Change Selected Products')

    @staticmethod
    def enable_periodic_task(obj, enable):
        if obj.periodic_task is not None:
            obj.periodic_task.enabled = enable
            obj.periodic_task.save()


admin.site.register(Product, ProductAdmin)
