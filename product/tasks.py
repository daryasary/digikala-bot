from __future__ import absolute_import, unicode_literals
from product.models import Product
from crawler.product_requests_crawler import ProductRequestsCrawler
from celery.utils.log import get_task_logger
from sellers.celery_app import app
from utils.system_control import is_celery_enable, get_queue_length
from seller.models import Seller

logger = get_task_logger(__name__)


@app.task(queue='RequestQ')
def crawl_product_price(product_pk):
    if is_celery_enable():
        product = Product.objects.filter(pk=product_pk, is_active=True).first()
        if product is not None:
            crawler = ProductRequestsCrawler(product)
            crawler.check_position()
        else:
            logger.info('Could Not Found Product With PK %d or Is Not Active', product_pk)


@app.task(queue='CrawlQ')
def crawl_seller_products():
    if not is_celery_enable():
        logger.error("Celery is not enabled")
        return False
    if bool(get_queue_length('RequestQ')):
        logger.info("RequestQ is not empty")
        return False
    seller = Seller.objects.first()
    if seller is None:
        return False

    products = Product.objects.order_by('-id').filter(seller_id=seller.pk, is_active=True)
    logger.info("Started To Crawl Seller with Id: %s\tProducts count: %s" % (seller.pk, products.count()))
    for product in products:
        crawl_product_price.delay(product.pk)
    return True


@app.task(queue='DetailQ')
def crawl_product_detail(product_pk):
    if is_celery_enable():
        product = Product.objects.filter(pk=product_pk, is_active=True).first()
        if product is not None:
            c = ProductRequestsCrawler(product, from_admin=True)
            product.title = c.get_product_title()
            (p_id, p_uid) = c.get_product_uid()
            product.product_id = p_id
            product.product_uid = p_uid
            lowest_price = c.lowest_price()
            if lowest_price is not None:
                product.lowest_spotted_price = lowest_price.price
            product.save()


@app.task(queue='DetailQ')
def crawl_new_products_detail():
    if is_celery_enable():
        products = Product.objects.filter(title__isnull=True).values_list('id', flat=True)
        for pid in products:
            print("Fetch data for PK:" + str(pid))
            crawl_product_detail.delay(pid)

        if not len(products):
            print("Nothing to crawl...")
