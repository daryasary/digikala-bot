from django.contrib.auth.decorators import user_passes_test
from django.http import HttpResponse


@user_passes_test(lambda u: u.is_staff)
def flower_view(request):
    response = HttpResponse()
    path = request.get_full_path()
    path = path.replace('flower', 'monitor', 1)
    response['X-Accel-Redirect'] = path
    return response
