import redis
from django.conf import settings

r = redis.Redis(charset="utf-8", decode_responses=True, host='localhost', port=6379, db=0)


def is_celery_enable():
    return True
    # is_enable = r.get(settings.CELERY_ENV_KEY)
    # if is_enable is not None:
    #     return is_enable == '1'
    # else:
    #     enable_celery()
    #     return True


def enable_celery():
    r.set(settings.CELERY_ENV_KEY, '1')


def disable_celery():
    r.set(settings.CELERY_ENV_KEY, '0')


def get_queue_length(queue_name):
    from sellers.celery_app import app as celery_app
    with celery_app.pool.acquire(block=True) as conn:
        return conn.default_channel.client.llen(queue_name)
