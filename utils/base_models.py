from django.db import models
from django_jalali.db import models as jmodels
from django.utils.translation import ugettext as _
from django.contrib import admin


class BaseModel(models.Model):
    created_date = jmodels.jDateTimeField(auto_now_add=True, verbose_name=_('Created Date'))
    updated_date = jmodels.jDateTimeField(auto_now=True, verbose_name=_('Updated Date'))

    class Meta:
        abstract = True


class BadeAdmin(admin.ModelAdmin):
    readonly_fields = ('created_date', 'updated_date', )

