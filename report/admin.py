from babel.numbers import format_decimal
from django.contrib import admin
from django.utils import timezone
from report.models import Report
from utils.base_models import BadeAdmin
from django.utils.translation import ugettext as _


class ReportAdmin(BadeAdmin):
    list_display = ('product', 'jupdated_date', 'product_variant_id', 'formatted_market_lowest_price',
                    'formatted_offset_price', 'formatted_reduced_price', 'divided_reduced_price_in_rials',
                    'formatted_round_reduced_price', 'status')
    exclude = ()
    list_filter = ('product', )
    search_fields = ('product', 'product_variant_id', 'product_product_uid', 'product_product_id')
    ordering = ('-created_date',)

    def get_readonly_fields(self, request, obj=None):
        return [f.name for f in self.model._meta.fields]

    def jupdated_date(self, obj):
        return timezone.localtime(obj.updated_date).strftime("%Y/%m/%d %H:%M:%S")

    jupdated_date.short_description = _('Updated Date')

    def formatted_market_lowest_price(self, obj):
        return format_decimal(obj.market_lowest_price, locale='en_US')

    formatted_market_lowest_price.short_description = _('Market Lowest Price')

    def formatted_offset_price(self, obj):
        return format_decimal(obj.offset_price, locale='en_US')

    formatted_offset_price.short_description = _('Price Offset')

    def formatted_reduced_price(self, obj):
        return format_decimal(obj.reduced_price, locale='en_US')

    formatted_reduced_price.short_description = _('Reduced Price')

    def formatted_round_reduced_price(self, obj):
        return format_decimal(obj.round_reduced_price, locale='en_US')

    formatted_round_reduced_price.short_description = _('Lowest Price')


admin.site.register(Report, ReportAdmin)

