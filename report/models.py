from django.db import models
from utils.base_models import BaseModel
from product.models import Product
from django.utils.translation import ugettext as _


class Report(BaseModel):
    STATUS_CHOICES = (
        (0, _('Not Changed')),
        (1, _('Changing')),
        (2, _('Change Success')),
        (3, _('Is Lowest Price No Need To Change')),
    )
    product = models.ForeignKey(null=True, blank=True, to=Product, verbose_name=_('Product'), on_delete=models.SET_NULL)
    product_variant_id = models.PositiveIntegerField(verbose_name=_('Product Variant ID'))
    market_lowest_price = models.PositiveIntegerField(verbose_name=_('Market Lowest Price'))
    offset_price = models.PositiveIntegerField(verbose_name=_('Offset Price'))
    minimum_price = models.PositiveIntegerField(verbose_name=_('Minimum Price'))
    reduced_price = models.PositiveIntegerField(verbose_name=_('Reduced Price'))
    divided_reduced_price_in_rials = models.PositiveIntegerField(verbose_name=_('Divided Price'))
    round_reduced_price = models.PositiveIntegerField(verbose_name=_('Round Reduced Price'))
    status = models.SmallIntegerField(choices=STATUS_CHOICES, default=0, verbose_name=_('Status'))

    def __str__(self):
        return self.product.product_uid

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        self.product_variant_id = self.product.variant_id
        super(Report, self).save(force_insert=False, force_update=False, using=None, update_fields=None)

    class Meta:
        verbose_name = _('Report')
        verbose_name_plural = _('Reports')
