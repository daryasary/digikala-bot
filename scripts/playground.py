from seller.models import Seller
from product.models import Product
from crawler.product_crawler import ProductCrawler
from crawler.seller_panel_crawler import SellerPanelCrawler


def play(seller_id):
    seller = Seller.objects.filter(id=seller_id).first()
    if seller is not None:
        for product in seller.products.all():
            print(product.product_url)
            crawler = ProductCrawler(product)
            crawler.check_prices()


def play_reduce_price(product_pk):
    product = Product.objects.filter(pk=product_pk).first()
    if product is not None:
        print(product.product_url)
        crawler = SellerPanelCrawler(product)
        crawler.reduce_price()


