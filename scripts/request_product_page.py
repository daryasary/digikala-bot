from urllib.parse import urlparse
from bs4 import BeautifulSoup
import requests
import re

r = requests.get('https://www.digikala.com/product/dkp-512197')
soup = BeautifulSoup(r.content, features="html.parser")
out_of_stock = soup.select_one('div.c-product__status-bar.c-product__status-bar--out-of-stock')
print(out_of_stock)


