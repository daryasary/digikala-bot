FROM ubuntu:16.04
MAINTAINER Sajjad Aboutalebi <aboutsajjad@gmail.com>
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
RUN apt-get update
RUN apt-get install -y wget
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN echo 'deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main' | tee /etc/apt/sources.list.d/google-chrome.list
RUN apt-get update
RUN apt-get install -y unzip google-chrome-stable python3 python3-pip locales
RUN dpkg-reconfigure locales
ENV LANG C.UTF-8
ADD requirements.txt /code/
RUN pip3 install -r requirements.txt
ADD . /code/

