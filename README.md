# Sellers


#### Start Celery Worker With Concurrency

    celery -A sellers worker -l info -B -E  --concurrency=10 -n worker2 -Ofair --scheduler django_celery_beat.schedulers:DatabaseScheduler --loglevel=info -f celery.log

### Chrome Driver
    
    https://chromedriver.storage.googleapis.com/2.41/chromedriver_linux64.zip

### Install Chrome

Add Key:

    wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -

Set repository:

    echo 'deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main' | sudo tee /etc/apt/sources.list.d/google-chrome.list

Install package:

    sudo apt-get update 
    sudo apt-get install google-chrome-stable


##### SetUp DataBase
    CREATE DATABASE sellers_database;
    CREATE USER sellers WITH PASSWORD 'f~=8<Ag)7Q.C)%)p';
    ALTER ROLE sellers SET client_encoding TO 'utf8';
    ALTER ROLE sellers SET default_transaction_isolation TO 'read committed';
    ALTER ROLE sellers SET timezone TO 'Asia/Tehran';
    GRANT ALL PRIVILEGES ON DATABASE sellers_database TO sellers;
