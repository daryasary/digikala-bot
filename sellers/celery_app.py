from __future__ import absolute_import, unicode_literals
import os
from celery import Celery
from django.conf import settings

# set the default Django settings module for the 'celery' program.

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'sellers.settings.prod')

app = Celery('sellers')

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.ONCE = settings.CELERY_ONCE
app.config_from_object('django.conf:settings', namespace='CELERY')

# Load task modules from all registered Django app configs.

app.conf.beat_schedule = {
    'crawl-seller-products': {
        'task': 'product.tasks.crawl_seller_products',
        'schedule': 300.0,
        'options': {'queue': 'CrawlQ'}
    },
    'crawl-new-products-detail': {
        'task': 'product.tasks.crawl_new_products_detail',
        'schedule': 30.0,
        'options': {'queue': 'DetailQ'}
    },
}
app.autodiscover_tasks()


@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))
