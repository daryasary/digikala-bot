"""sellers URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

from product.views import flower_view

admin.site.site_header = "Sellers Admin"
admin.site.site_title = "Sellers Admin Portal"
admin.site.index_title = "Sellers Portal"

urlpatterns = [
    path('jet/', include('jet.urls', 'jet')),
    path('jet/dashboard/', include('jet.dashboard.urls', 'jet-dashboard')),
    path('', admin.site.urls),
    path('system/', include('seller.urls', namespace='seller')),
    path(r'flower/', flower_view),

]


urlpatterns = urlpatterns + static(
    settings.STATIC_URL,
    document_root=settings.STATIC_ROOT
)
