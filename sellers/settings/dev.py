from .base import *
from .local_settings import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*']

# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases
try:
    DATABASES = DATABASES_CONFIG
except NameError:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
        }
    }

DJANGO_SETTINGS = 'sellers.settings.dev'

CELERY_BROKER_URL = 'redis://redis:6379/{}'.format(REDIS_DB)

