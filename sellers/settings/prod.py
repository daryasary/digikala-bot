from .base import *
from .local_settings import *

DEBUG = False
DEBUG_PROPAGATE_EXCEPTIONS = False

ALLOWED_HOSTS = ['*']

# Database

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.postgresql',
#         'NAME': os.environ.get('DB_NAME', ''),
#         'USER': os.environ.get('DB_USERNAME', ''),
#         'PASSWORD': os.environ.get('DB_PASSWORD', ''),
#         'HOST': os.environ.get('DB_HOST', 'postgres'),
#         'PORT': os.environ.get('DB_PORT', ''),
#         'ATOMIC_REQUESTS': True,
#         # 'CONN_MAX_AGE': 5,
#     },
# }

DATABASES = DATABASES_CONFIG

DJANGO_SETTINGS = 'sellers.settings.prod'

CELERY_BROKER_URL = 'redis://redis:6379/{}'.format(REDIS_DB)
