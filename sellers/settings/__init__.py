import os

DEPLOYMENT = os.environ.get('DEPLOYMENT', 'PROD')
print(DEPLOYMENT)
if DEPLOYMENT == 'PROD':
    from .prod import *
if DEPLOYMENT is None:
    from .dev import *
