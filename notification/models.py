from django.db import models
from utils.base_models import BaseModel
from product.models import Product
from django.utils.translation import ugettext as _


class Notification(BaseModel):
    STATUS_CHOICES = (
        (0, _('Emailed')),
        (1, _('Text Messaged')),
        (2, _('Revoked')),
    )
    product = models.ForeignKey(to=Product, on_delete=models.CASCADE, verbose_name=_('Product'))
    status = models.PositiveSmallIntegerField(choices=STATUS_CHOICES, default=3, verbose_name=_('Status'))
