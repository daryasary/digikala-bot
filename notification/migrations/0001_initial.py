# Generated by Django 2.0 on 2019-01-31 20:08

from django.db import migrations, models
import django.db.models.deletion
import django_jalali.db.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('product', '0010_auto_20180910_0321'),
    ]

    operations = [
        migrations.CreateModel(
            name='Notification',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', django_jalali.db.models.jDateTimeField(blank=True, editable=False, verbose_name='Created Date')),
                ('updated_date', django_jalali.db.models.jDateTimeField(blank=True, editable=False, verbose_name='Updated Date')),
                ('status', models.PositiveSmallIntegerField(choices=[(0, 'Emailed'), (1, 'Text Messaged'), (2, 'Revoked')], default=3, verbose_name='Status')),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='product.Product', verbose_name='Product')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
