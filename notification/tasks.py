from __future__ import absolute_import, unicode_literals
from django.core.mail import send_mail
from sellers.celery_app import app
from product.models import Product
from notification.models import Notification


@app.task(queue='EmailQ')
def alert(product_pk):
    product = Product.objects.get(pk=product_pk)
    # send_mail(
    #     'Product: ' + str(product.product_id) + ' - ' + str(product.variant_id) + ' Is Out Of Stock',
    #     'URL: ' + str(product.product_url),
    #     'from@example.com',
    #     ['to@example.com'],
    #     fail_silently=False,
    # )
    # Notification.objects.create(product=product, status=1)


@app.task(queue='EmailQ')
def check_alert(product_pk):
    pass
    # notification = Notification.objects.order_by('-id').filter(product_id=product_pk, status__lte=1).first()
    # if notification is None:
    #     alert.delay(product_pk)


@app.task(queue='EmailQ')
def revoke_alert(product_pk):
    pass
    # Notification.objects.order_by('-id').filter(product_id=product_pk).update(status=2)
