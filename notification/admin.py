from django.contrib import admin
from notification.models import Notification
from utils.base_models import BadeAdmin


class NotificationAdmin(BadeAdmin):
    list_display = ('status', 'product')


admin.site.register(Notification, NotificationAdmin)
